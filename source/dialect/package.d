/++
 +  This is a package file for all the IRC modules. It only publicly imports
 +  them and does not contain any code itself.
 +/
module dialect;

public import dialect.defs;
public import dialect.parsing;
public import dialect.common;
